import os
import urllib
import uuid
import requests
from io import BytesIO
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig

import numpy as np
from PIL import Image
import torch
import torchvision.transforms as transforms

from mnistclassifier.config import Config
from mnistclassifier.deeplearning.predictor import Predictor


class IC_PredictController(Controller):

    def predict_img(self, project, img):
        config = Config(configs=os.path.join(
            AppConfig.DATA_FOLDER, project, 'config.yml'))
        classes = config.get()['classes']
        width = config.get()['img_width']
        height = config.get()['img_height']
        try:
            width = int(width)
            height = int(height)
        except:
            width = 32
            height = 32

        preprocess = transforms.Compose([
            transforms.Resize((height, width)),
            # transforms.CenterCrop(32),
            transforms.ToTensor(),
            #transforms.Lambda(lambda x: torch.cat([x, x, x], 0)),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
        img = preprocess(img)

        predictor = Predictor(config)
        predictor.load()
        prediction, top_probabilities, top_classes = predictor.predict(
            img.unsqueeze_(0))

        top_p = top_probabilities.detach().numpy().tolist()[0]
        top_p = np.nan_to_num(top_p).tolist()
        top_c = top_classes.detach().numpy().tolist()[0]
        top_c_names = [(classes[int(c)] if int(c) < len(classes) else 'unknown')
                       for c in top_c]
        top_c = top_c_names
        class_name = (classes[prediction[0]] if int(prediction[0]) < len(classes) else 'unknown')
        
        return class_name, top_p, top_c

    def cache_img(self, img):
        img_io = BytesIO()
        img.save(img_io, 'JPEG', quality=70)
        id = str(uuid.uuid4())+'.jpg'
        redis = current_app.config.get('SESSION_REDIS')
        redis.setex(id, 600, img_io.getvalue())
        return id

    route('/predict/<string:project>/<path:url>', methods=['GET'])

    def predict(self, project, url):
        url = urllib.parse.unquote(url)
        # source: https://stackoverflow.com/questions/7391945/how-do-i-read-image-data-from-a-url-in-python
        response = requests.get(url)
        img = Image.open(BytesIO(response.content)).convert('RGB')
        id = self.cache_img(img)
        prediction, top_p, top_c = self.predict_img(project, img)
        print('predict', url, prediction)
        return jsonify({'id': id, 'prediction': prediction, 'probabilities': top_p, 'predictions': top_c})

    @route('/predict/<string:project>', methods=['POST'])
    def post_predict(self, project):
        print(project)
        if 'filepond' not in request.files:
            return abort(404)
        file = request.files['filepond']
        print(file)
        if file.filename == '':
            return abort(404)
        img = Image.open(file.stream)
        id = self.cache_img(img)
        prediction, top_p, top_c = self.predict_img(project, img)
        print('predict', file.filename, prediction, top_p, top_c)
        return jsonify({'id': id, 'project': project, 'filename': file.filename,
                        'prediction': prediction, 'probabilities': top_p, 'predictions': top_c})

    @route('/image/<string:id>')
    def image(self, id):
        redis = current_app.config.get('SESSION_REDIS')
        cached = redis.get(id)
        if not cached:
            return abort(404)
        img_io = BytesIO()
        img = Image.open(BytesIO(cached))
        img.save(img_io, 'JPEG', quality=70)
        img_io.seek(0)
        return send_file(img_io, mimetype='image/jpeg')

    @route('/thumbnail/<string:id>')
    def thumbnail(self, id):
        redis = current_app.config.get('SESSION_REDIS')
        cached = redis.get(id)
        if not cached:
            return abort(404)
        img_io = BytesIO()
        img = Image.open(BytesIO(cached))
        img.thumbnail((128, 128), Image.ANTIALIAS)
        img.save(img_io, 'JPEG', quality=70)
        img_io.seek(0)
        return send_file(img_io, mimetype='image/jpeg')
