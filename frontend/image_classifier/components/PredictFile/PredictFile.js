import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import './predict-file.scss'
import { v1 } from 'api'
import { FilePond, File, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import { storage } from 'comps/project'
import { ClassifiedImage } from 'comps/image_classifier/components'

registerPlugin(FilePondPluginImagePreview);


function classifier(uri) {
  return v1(`/image_classifier${uri}`)
}

class PredictFile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // Set initial files
      files: [],
      predictions: [],
      prediction: ''
    };
  }

  handleInit() {
  }

  render() {
    const { prediction, predictions } = this.state
    const server = {
      url: classifier(`/predict/${storage.getProject()}`),
      process: {
        onload: (res) => {
          const res_obj = JSON.parse(res)
          const prediction = {
            id: res_obj['id'],
            prediction: res_obj['prediction'],
            probabilities: res_obj['probabilities'],
            classes: res_obj['predictions']
          }
          const predictions = this.state.predictions
          predictions.push(prediction)
          this.setState({ prediction: prediction['prediction'], predictions: predictions })
          return res;
        }
      }
    }
    return (
      <div className="PredictFile">

        {/* Pass FilePond properties as attributes */}
        <FilePond ref={ref => this.pond = ref}
          allowMultiple={true}
          maxFiles={100}
          server={server}
          oninit={() => this.handleInit()}
          onupdatefiles={(fileItems) => {
            // Set current file objects to this.state

            this.setState({
              files: fileItems.map(fileItem => fileItem.file)
            });
          }}>

          {/* Update current files  */}
          {this.state.files.map(file => (
            <File key={file} src={file} origin="local" />
          ))}

        </FilePond>
        <label>Results:</label>
        <ul className="predictions">
          {predictions.map((pred, i) => {
            const file = this.state.files[i]
            return (
              <li key={i} >
                  <ClassifiedImage id={pred['id']} prediction={pred['prediction']} probabilities={pred['probabilities']} classes={pred['classes']} />
              </li>
            )
          })}
        </ul>

      </div>
    );
  }
}

export default compose(
)(PredictFile)
