import { listClassifierDataSets } from 'comps/image_classifier/actions'


export const KEY = 'classifierdatasets'

const initialState = {
  isLoading: false,
  isLoaded: false,
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { datasets } = payload || {}
  switch (type) {
    case listClassifierDataSets.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listClassifierDataSets.SUCCESS:
      return {
        ...state,
        datasets,
        isLoaded: true,
      }

    case listClassifierDataSets.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listClassifierDataSets.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectClassifierDataSets = (state) => state[KEY]
export const selectClassifierDataSetsList = (state) => {
  const datasets = selectClassifierDataSets(state)
  return datasets.datasets
}
