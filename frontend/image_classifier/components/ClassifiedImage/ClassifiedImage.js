import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import './classified-image.scss'
import { storage } from 'comps/project'
import { v1 } from 'api'

class Image extends React.Component {

  constructor(props) {
    super(props);
    this.state = {selected: false};
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.setState({ selected: !this.state.selected });
  }

  render() {
    const { isLoaded, error, id, prediction, probabilities, classes } = this.props
    const { selected } = this.state
    if (!id) {
      return null
    }
    return (
      selected ? <div className='selected-image'>
        <p>{prediction}</p>
        <img onClick={this.onClick} src={v1(`/image_classifier/image/${id}`)} />

      </div> :
        <div onClick={this.onClick} className='classified-image'>
          <img src={v1(`/image_classifier/thumbnail/${id}`)} />
          <ol>
            <li>{probabilities[0]}-{classes[0]}</li>
            <li>{probabilities[1]}-{classes[1]}</li>
            <li>{probabilities[2]}-{classes[2]}</li>
          </ol></div>
    )
  }
}

export default compose(
)(Image)
