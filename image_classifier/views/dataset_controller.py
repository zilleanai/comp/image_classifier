# source: https://stackoverflow.com/questions/23718236/python-flask-browsing-through-directory-with-files
# https://stackoverflow.com/questions/27337013/how-to-send-zip-files-in-the-python-flask-framework

import os
import json
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import Controller

from mnistclassifier.data import available

class IC_DatasetController(Controller):
    def datasets(self):
        datasets = ['dataset.yml']
        datasets.extend(available())
        resp = jsonify(datasets=datasets)
        return resp
