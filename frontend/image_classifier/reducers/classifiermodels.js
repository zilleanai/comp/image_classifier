import { listClassifierModels } from 'comps/image_classifier/actions'


export const KEY = 'classifiermodels'

const initialState = {
  isLoading: false,
  isLoaded: false,
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { models } = payload || {}
  switch (type) {
    case listClassifierModels.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listClassifierModels.SUCCESS:
      return {
        ...state,
        models,
        isLoaded: true,
      }

    case listClassifierModels.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listClassifierModels.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectClassifierModels = (state) => state[KEY]
export const selectClassifierModelsList = (state) => {
  const models = selectClassifierModels(state)
  return models.models
}
