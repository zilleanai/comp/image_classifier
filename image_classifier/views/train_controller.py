import os
import yaml
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig

from mnistclassifier.config import Config

from ..tasks import train_async_task
from bundles.project.models import Project
from bundles.project.services import ProjectManager
from bundles.tag.services import TagManager
from bundles.file.db import FileDB
from bundles.file.models import File
from time import sleep

class IC_TrainController(Controller):

    project_manager: ProjectManager = injectable
    tag_manager: TagManager = injectable

    def create_dataset(self, project, classes, path):
        dataset = {'classes': classes, 'train': {}, 'test': {}}
        for c in classes:
            # select file.path, file.filename from files.project==project and class in files.tags
            files = FileDB(project).getFilePathsByTag(c)
            dataset['train'][c] = [os.path.join(
                file[0], file[1]) for file in files]
        with open(os.path.join(path, 'dataset.yml'), 'w') as outfile:
            yaml.dump(dataset,
                      outfile, default_flow_style=False)

    @route('/train', methods=['POST'])
    def classifier_train(self):
        project = request.json['project']
        classes = request.json['classes']
        model = request.json['model']
        dataset = request.json['dataset']
        img_width = request.json['img_width']
        img_height = request.json['img_height']
        try:
            img_width = int(img_width)
            img_height = int(img_height)
        except:
            img_width = 32
            img_height = 32
        if dataset == 'cifar10':
            if len(classes) < 10:
                classes = ['airplane', 'automobile', 'bird', 'cat', 'deer',
                           'dog', 'frog', 'horse', 'ship', 'truck']
            img_width = 32
            img_height = 32

        BASE_DIR = os.path.join(AppConfig.DATA_FOLDER, project)
        os.makedirs(BASE_DIR, exist_ok=True)
        config = Config({project: {}})
        config.get()['model'] = model
        config.get()['dataset'] = dataset
        config.get()['batch_size'] = 100
        config.get()['learn_rate'] = 0.1
        config.get()['epochs'] = 2
        config.get()['img_width'] = img_width
        config.get()['img_height'] = img_height
        config.get()['classes'] = classes

        if dataset == 'dataset.yml':
            self.create_dataset(project, classes, BASE_DIR)
        config.save(os.path.join(BASE_DIR, 'config.yml'))

        train_async_task.delay({'project': project,
                                'DATA_FOLDER': AppConfig.DATA_FOLDER})
        sleep(1)
        return jsonify(success=True)
