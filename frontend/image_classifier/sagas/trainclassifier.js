import { call, put, takeLatest } from 'redux-saga/effects'

import { trainClassifier } from 'comps/image_classifier/actions'
import { createRoutineFormSaga } from 'sagas'
import ClassifierApi from 'comps/image_classifier/api'


export const KEY = 'train_classifier'

export const trainClassifierSaga = createRoutineFormSaga(
  trainClassifier,
  function *successGenerator(payload) {
    const response = yield call(ClassifierApi.trainClassifier, payload)
    yield put(trainClassifier.success(response))}
)

export default () => [
  takeLatest(trainClassifier.TRIGGER, trainClassifierSaga)
]
