import { createRoutine } from 'actions'

export const listClassifierModels = createRoutine('image_classifier/LIST_CLASSIFIER_MODELS')
export const listClassifierDataSets = createRoutine('image_classifier/LIST_CLASSIFIER_DATASETS')
export const trainClassifier = createRoutine('image_classifier/TRAIN_CLASSIFIER')
