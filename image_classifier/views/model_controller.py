
import os
import json
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import Controller, route
from backend.config import Config as AppConfig
from mnistclassifier.deeplearning.models import available


class IC_ModelController(Controller):

    @route('/models', defaults={'project': ''})
    @route('/models/<string:project>')
    def models(self, project=''):
        if not project:
            return jsonify(models=available())
        models = available()
        BASE_DIR = os.path.join(AppConfig.DATA_FOLDER, project)
        py_files = [f for f in os.listdir(BASE_DIR) if f.endswith('.py')]
        models.extend(py_files)
        resp = jsonify(models=models)
        return resp
