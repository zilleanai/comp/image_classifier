import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'

import { listClassifierModels } from 'comps/image_classifier/actions'
import { selectClassifierModelsList } from 'comps/image_classifier/reducers/classifiermodels'
import Select from 'react-select';


import './classifier-model-list.scss'

class ClassifierModelList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    this.props.onChange({ value: selectedOption.value })
  }

  componentWillMount() {
    this.props.listClassifierModels.trigger()
  }

  itemsToOptions(items) {
    var options = []
    items.forEach(function (value, key) {
      options.push({ value: value, label: value })
    }, items)
    return options
  }

  render() {
    const { isLoaded, error, models } = this.props
    if (!isLoaded) {
      return null
    }
    const options = this.itemsToOptions(models.models)
    return (
      <Select
        value={this.state.selectedOption}
        onChange={this.handleChange}
        defaultValue={options[0]}
        name="models"
        options={options}
        className="basic-multi-select"
        classNamePrefix="select"
      />
    )
  }
}

const withReducer = injectReducer(require('comps/image_classifier/reducers/classifiermodels'))

const withSaga = injectSagas(require('comps/image_classifier/sagas/classifiermodels'))

const withConnect = connect(
  (state) => {
    const models = selectClassifierModelsList(state)
    const isLoaded = !!models
    return {
      models,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ listClassifierModels }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ClassifierModelList)
