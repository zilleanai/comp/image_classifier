import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm, formValues } from 'redux-form'
import formActions from 'redux-form/es/actions'
const { reset } = formActions
import Helmet from 'react-helmet'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { InfoBox, PageContent } from 'components'
import { ClassifierModelList, ClassifierDataSetList, PredictFile } from 'comps/image_classifier/components'
import { TagSelect } from 'comps/tag/components'
import { trainClassifier } from 'comps/image_classifier/actions'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';

const FORM_NAME = 'trainClassifier'

class ImageClassifier extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      model: '',
      dataset: '',
      classes: [],
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.imageclassifier-page',
          intro: 'This page allows to train and predict images to classes.',
        },
        {
          element: '.model',
          intro: 'Select a model for classification here. Resnet18, vgg16 and cifar10 are available here. Cifar10 allows to train for 10 classes.',
        },
        {
          element: '.dataset',
          intro: 'Select a dataset to train the model with. Cifar10 is a dataset with images of 10 classes. Dataset.yml will create a config file, containing path to images tagged with given classes.',
        },
        {
          element: '.classes',
          intro: 'Select classes here which will be used to represent the predictions.',
        },
        {
          element: '.image-size',
          intro: 'Images will be resized to given size. On training and prediction the selected size has to be the same.',
        },
        {
          element: '.button-primary',
          intro: 'Pressing this button starts a retraining.',
        }, {
          element: '.PredictFile',
          intro: 'Paste images here to make an prediction.',
        },
      ],
    }
    this.handleModels = this.handleModels.bind(this);
    this.handleDataSets = this.handleDataSets.bind(this);
    this.handleClassesChange = this.handleClassesChange.bind(this);
  }

  number = (value) =>
    value && isNaN(Number(value)) ? 'Must be a number' : undefined

  minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined

  minValue1 = this.minValue(1)

  handleModels = ({ value: model }) => {
    this.setState({ model: model });
  }

  handleDataSets = ({ value: dataset }) => {
    this.setState({ dataset: dataset });
  }

  handleClassesChange({ value }) {
    this.setState({ classes: value });
  }

  renderModel = ({ input: { onChange } }) => {
    return (<ClassifierModelList
      onChange={onChange} />)
  }

  renderDataSet = ({ input: { onChange } }) => {
    return (<ClassifierDataSetList
      onChange={onChange} />)
  }

  renderClasses = ({ input: { onChange } }) => {
    return (<TagSelect
      onChange={onChange} />)
  }

  renderField = ({
    input,
    label,
    type,
    meta: { touched, error, warning }
  }) => {
    return (
      <div>
        <label>{label}</label>
        <div>
          <input {...input} placeholder={label} type={type} />
          {touched &&
            ((error && <span>{error}</span>) ||
              (warning && <span>{warning}</span>))}
        </div>
      </div>
    )
  }

  render() {
    const { isLoaded, error, handleSubmit, pristine, submitting } = this.props

    if (!isLoaded) {
      return null
    }
    const { stepsEnabled, steps, initialStep } = this.state;
    return (
      <PageContent className='imageclassifier-page'>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Classifier</title>
        </Helmet>
        <h1>Classifier!</h1>
        <form onSubmit={handleSubmit(trainClassifier)}>
          <div className='model'>
            <label>Model</label>
            <Field name='model' component={this.renderModel} onChange={this.handleModelChange}
              value={this.state.classes} />

          </div>
          <div className='dataset'>
            <label>Dataset</label>
            <Field name='dataset' component={this.renderDataSet} onChange={this.handleDataSetChange}
              value={this.state.classes} />
          </div>
          <div className='classes' >
            <label>Classes</label>
            <Field name='classes' component={this.renderClasses} onChange={this.handleClassesChange}
              value={this.state.classes} />
          </div>
          <div className='image-size'>
            <Field
              name="img_width"
              type="number"
              component={this.renderField}
              label="Image Width"
              validate={[this.number, this.minValue1]}
            />
            <Field
              name="img_height"
              type="number"
              component={this.renderField}
              label="Image Height"
              validate={[this.number, this.minValue1]}
            />
          </div>
          <div className="row">
            <button type="submit"
              className="button-primary"
              disabled={pristine || submitting}
            >
              {submitting ? 'Training...' : 'Train'}
            </button>
          </div>
        </form>
        <label>Predict</label>
        <PredictFile />
      </PageContent>)
  }
}

const withConnect = connect(
  (state) => {
    const isLoaded = true
    if (isLoaded) {
      return {
        isLoaded
      }
    } else {
      return {
        isLoaded: false
      }
    }
  },
  (dispatch) => bindRoutineCreators({}, dispatch),
)

const withSaga = injectSagas(require('comps/image_classifier/sagas/trainclassifier'))

const withForm = reduxForm({
  form: FORM_NAME,
})

export default compose(
  withSaga,
  withForm,
  withConnect,
)(ImageClassifier)
