# image_classifier

Image classifier component

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/image_classifier.git
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.image_classifier',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.image_classifier.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  ImageClassifier,
} from 'comps/image_classifier/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  ImageClassifier: 'ImageClassifier',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.ImageClassifier,
    path: '/imageclassifier',
    component: ImageClassifier,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.ImageClassifier} />
    ...
</div>
```