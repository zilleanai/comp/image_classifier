from flask_unchained import (controller, resource, func, include, prefix,
                             get, delete, post, patch, put, rule)

from .views import IC_DatasetController, IC_ModelController, IC_PredictController, IC_TrainController


routes = lambda: [
    prefix('/api/v1', [
        prefix('/image_classifier', [    
            controller(IC_DatasetController),
            controller(IC_ModelController),
            controller(IC_PredictController),
            controller(IC_TrainController),
        ]),
    ]),
]
