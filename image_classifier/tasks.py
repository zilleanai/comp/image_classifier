import os
import gc
from flask_unchained.bundles.celery import celery
from backend.config import Config as AppConfig

from mnistclassifier.deeplearning.trainer import Trainer
from mnistclassifier.config import Config
from multiprocessing import current_process


@celery.task(serializer='json')
def train_async_task(trainer_config):
    current_process()._config['daemon'] = False
    AppConfig.DATA_FOLDER = trainer_config['DATA_FOLDER']
    project = trainer_config['project']
    config = Config(configs=os.path.join(
        AppConfig.DATA_FOLDER, project, 'config.yml'))
    trainer = Trainer(config)
    trainer.load()
    trainer.train()
    trainer.save()
    print(trainer.performance())
    del trainer
    gc.collect()
