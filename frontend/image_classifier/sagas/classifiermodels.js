import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listClassifierModels } from 'comps/image_classifier/actions'
import ClassifierApi from 'comps/image_classifier/api'
import { selectClassifierModels } from 'comps/image_classifier/reducers/classifiermodels'

export const KEY = 'classifiermodels'


export const maybeListClassifierModelsSaga = function *() {
  const { isLoading, isLoaded } = yield select(selectClassifierModels)
  if (!(isLoaded || isLoading)) {
    yield put(listClassifierModels.trigger())
  }
}

export const listClassifierModelsSaga = createRoutineSaga(
  listClassifierModels,
  function *successGenerator() {
    const models = yield call(ClassifierApi.listClassifierModels)
    yield put(listClassifierModels.success({
      models,
    }))
  },
)

export default () => [
  takeEvery(listClassifierModels.MAYBE_TRIGGER, maybeListClassifierModelsSaga),
  takeLatest(listClassifierModels.TRIGGER, listClassifierModelsSaga),
]
