import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listClassifierDataSets } from 'comps/image_classifier/actions'
import ClassifierApi from 'comps/image_classifier/api'
import { selectClassifierDataSets } from 'comps/image_classifier/reducers/classifierdatasets'

export const KEY = 'classifierdatasets'


export const maybeListClassifierDataSetsSaga = function *() {
  const { isLoading, isLoaded } = yield select(selectClassifierDataSets)
  if (!(isLoaded || isLoading)) {
    yield put(listClassifierDataSets.trigger())
  }
}

export const listClassifierDataSetsSaga = createRoutineSaga(
  listClassifierDataSets,
  function *successGenerator() {
    const datasets = yield call(ClassifierApi.listClassifierDataSets)
    yield put(listClassifierDataSets.success({
      datasets,
    }))
  },
)

export default () => [
  takeEvery(listClassifierDataSets.MAYBE_TRIGGER, maybeListClassifierDataSetsSaga),
  takeLatest(listClassifierDataSets.TRIGGER, listClassifierDataSetsSaga),
]
