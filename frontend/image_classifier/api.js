import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function classifier(uri) {
  return v1(`/image_classifier${uri}`)
}

export default class Classifier {

  static listClassifierModels() {
    return get(classifier(`/models/${storage.getProject()}`))
  }

  static listClassifierDataSets() {
    return get(classifier('/datasets'))
  }

  static trainClassifier({ classes, model, dataset, img_width, img_height }) {
    return post(classifier('/train'), {
      project: storage.getProject(),
      classes: classes.value, model: model.value,
      dataset: dataset.value, img_width: img_width, img_height: img_height
    })
  }

}
