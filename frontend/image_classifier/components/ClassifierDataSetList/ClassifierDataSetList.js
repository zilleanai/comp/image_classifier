import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'

import { listClassifierDataSets } from 'comps/image_classifier/actions'
import { selectClassifierDataSetsList } from 'comps/image_classifier/reducers/classifierdatasets'
import Select from 'react-select';


import './classifier-dataset-list.scss'

class ClassifierDataSetList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    this.props.onChange({ value: selectedOption.value })
  }

  componentWillMount() {
    this.props.listClassifierDataSets.trigger()
  }

  itemsToOptions(items) {
    var options = []
    items.forEach(function (value, key) {
      options.push({ value: value, label: value })
    }, items)
    return options
  }

  render() {
    const { isLoaded, error, datasets } = this.props
    if (!isLoaded) {
      return null
    }
    const options = this.itemsToOptions(datasets.datasets)
    return (
      <Select
        value={this.state.selectedOption}
        onChange={this.handleChange}
        defaultValue={options[0]}
        name="datasets"
        options={options}
        className="basic-multi-select"
        classNamePrefix="select"
      />
    )
  }
}

const withReducer = injectReducer(require('comps/image_classifier/reducers/classifierdatasets'))

const withSaga = injectSagas(require('comps/image_classifier/sagas/classifierdatasets'))

const withConnect = connect(
  (state) => {
    const datasets = selectClassifierDataSetsList(state)
    const isLoaded = !!datasets
    return {
      datasets,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ listClassifierDataSets }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ClassifierDataSetList)
